# Prepared using this script:
# https://github.com/root-project/rootbench/blob/master/tools/upload2influxdb.py

import csv
import os

from generateSummary import generate_summary

def autodetect_dtype(x):
    # Raise a ValueError if it's empty and jump over that entry
    if x in ['', None]:
        raise ValueError
    # Try to convert to bool
    if x in ["true", "True"]:
        return True
    if x in ["false", "False"]:
        return False
    # Try to convert to float
    try:
        return float(x)
    except:
        pass
    # If no conversion works, return the inital value as string
    return str(x)

def parse_csv(csv_file, benchmark_type):
    lines = open(csv_file, 'r').readlines()
    idx = None
    status = "passed"
    for i, line in enumerate(lines):
        if line.startswith("name,") or line.startswith("id,"):
            idx = i
            break

    if idx == None:
        print(f"{benchmark_type} csv does not have correct layout")
        # CSV file not having a correct layout means that the benchmark failed
        status = "failed"

    # Drop the first lines
    lines = lines[idx:]

    # Read data
    reader = csv.reader(lines)
    header = next(reader)
    print(f"Extract fields from {benchmark_type}: {header}")
    if not (header[0] == "name" or header[0] == "id"):
        print(f"{benchmark_type} header does not have correct layout")
        # CSV file not having a correct layout means that the benchmark failed
        status = "failed"

    data = []
    for row in reader:
        d_raw = {h: row[i] for i, h in enumerate(header)}
        d = {}
        for k, v in d_raw.items():
            try:
                d[k] = autodetect_dtype(v)
            except ValueError:
                pass
        data.append(d)
    print(f"Extracted {len(data)} measurements")

    return data, status

def get_duration(data, benchmark_type, status):
    duration_summary = {}
    if status == "passed":
        for row in data:
            name = row["name"]
            duration_summary[name] = {}
            if benchmark_type == "pytest":
                duration = row["duration"]
                unit = "s"
            else:
                duration = row["real_time"]
                unit = row["time_unit"]
                cpu_time = row["cpu_time"]
                duration_summary[name]["cpu_time"] = f"{cpu_time}"
            print(f"Duration of benchmark {name}: {duration}{unit}")

            duration_summary[name]["duration"] = f"{duration}"
            duration_summary[name]["unit"] = f"{unit}"
            duration_summary[name]["status"] = f"{status}"
    else:
        duration_summary["status"] = f"{status}"

    return duration_summary

def get_benchmark_names():
    benchmarks_file = "/hep-analysis-workloads/root/root/benchmarks.txt"
    # benchmarks_file = "/Users/domi/Studia/CERN/hep-analysis-workloads/root/root/benchmarks.txt"
    outfile = open(benchmarks_file, 'r')
    lines = outfile.readlines()
    benchmark_names = []

    for line in lines:
        words = line.split()
        benchmark_names.append(words[0])
    
    return benchmark_names

def get_benchmark_name(benchmark_names, csv_file):
    for name in benchmark_names:
        if name in csv_file:
            print(f"Benchmark name: {name}")
            return name

    return None

def main():
    basedir = "/hep-analysis-workloads/build_rootbench/build"
    # basedir = "/Users/domi/Studia/CERN/csv_files"
    benchmark_names = get_benchmark_names()
    benchmark_statuses = []
    duration_summaries = {}
    for root, dirs, files in os.walk(basedir, topdown=True):
        for filename in files:
            # Find csv files
            if not filename.endswith('.csv'):
                continue
            path = os.path.join(root, filename)
            print(f"Detected csv file {path}")

            benchmark_type = None
            if filename.startswith("rootbench-gbenchmark"):
                benchmark_type = "gbenchmark"
            elif filename.startswith("rootbench-pytest"):
                benchmark_type = "pytest"
            else:
                print("Failed to detect benchmark type of the csv file")

            benchmark_name = get_benchmark_name(benchmark_names, path)
            if benchmark_name is None:
                continue

            data, status = parse_csv(path, benchmark_type)
            benchmark_statuses.append(status)
            duration_summary = get_duration(data, benchmark_type, status)
            duration_summaries[benchmark_name] = duration_summary
    generate_summary(duration_summaries, benchmark_statuses)

if __name__ == "__main__":
    main()
