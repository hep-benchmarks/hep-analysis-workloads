import json

def convert_to_seconds(duration, unit):
    units = {"ms": 0.001, "ns": 0.000000001}

    return duration * units[unit]

def sum_durations(duration_summaries, weights):
    all_durations = 0
    for benchmark in duration_summaries:
        # Check if benchmark failed by checking if status key exists
        if "status" in duration_summaries[benchmark]:
            # Benchmark failed and will not be counted to duration time of workload
            continue
        for name in duration_summaries[benchmark]:
            if duration_summaries[benchmark][name]["unit"] != 's':
                converted_duration = convert_to_seconds(
                    float(duration_summaries[benchmark][name]["duration"]),
                    duration_summaries[benchmark][name]["unit"])
                all_durations += converted_duration * weights[benchmark]
            else:
                all_durations += float(duration_summaries[benchmark][name]["duration"]) * weights[benchmark]

    return all_durations

def get_weights():
    # benchmarks_file = "/Users/domi/Studia/CERN/hep-analysis-workloads/root/root/benchmarks.txt"
    benchmarks_file = "/hep-analysis-workloads/root/root/benchmarks.txt"
    outfile = open(benchmarks_file, 'r')
    lines = outfile.readlines()
    benchmark_weights = {}

    for line in lines:
        words = line.split()
        benchmark_weights[words[0]] = float(words[1])

    return benchmark_weights

def generate_summary(duration_summaries, benchmark_statuses):
    weights = get_weights()
    all_durations = sum_durations(duration_summaries, weights)

    # summary_file = "/Users/domi/Studia/CERN/summary.json"
    summary_file = "/results/summary.json"
    summary = {}
    summary["wl-scores"] = f"{all_durations}"
    summary["wl-stats"] = duration_summaries

    summary["log"] = "passed"
    if "failed" in benchmark_statuses:
        summary["log"] = "failed"

    with open(summary_file, 'w') as outfile:
        json.dump(summary, outfile, indent=4)
