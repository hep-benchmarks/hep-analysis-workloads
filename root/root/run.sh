#!/bin/bash

# Check if /results is writable
if [ ! -w "/results" ]
then
  echo "Directory /results is not writable"
  exit
fi

echo "Sourcing ROOT"
source /hep-analysis-workloads/build_root/build/install/bin/thisroot.sh

echo "Reading file benchmarks.txt"
input="/hep-analysis-workloads/root/root/benchmarks.txt"
while IFS= read -r benchmark weight
do
  echo "Running benchmark: $benchmark"
  cd /hep-analysis-workloads/build_rootbench/build/ && ctest -R $benchmark -VV
done < "$input"

python3 /hep-analysis-workloads/root/root/parseResults.py
