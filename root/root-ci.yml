---
stages:
- build_root
- build_rootbench
- build_standalone
- test_docker
- test_singularity

##########################
## Templates ############

.definition_build_image: &build
  image:
    name: gitlab-registry.cern.ch/ci-tools/docker-image-builder
    entrypoint: [""]
  script:
    - echo "Building docker image..."
    - echo "current commit is ${CI_COMMIT_SHA:0:8}"
    - echo "current branch is ${CI_COMMIT_BRANCH}"
    - echo "current tag is ${CI_COMMIT_TAG}"
    - echo "CI registry image is ${CI_REGISTRY_IMAGE}"
    - if [[ -z $DOCKERFILE ]]; then echo "ERROR variable DOCKERFILE is not defined "; exit 1; fi
    - if [[ -z $CONTEXT ]]; then echo "ERROR variable CONTEXT is not defined "; exit 1; fi
    - if [[ -z $IMAGE_NAME ]]; then echo "ERROR variable IMAGE_NAME is not defined "; exit 1; fi
    - /kaniko/executor --context $CONTEXT --dockerfile $DOCKERFILE --destination $IMAGE_NAME:$IMAGE_TAG

build_root: 
  stage: build_root
  before_script:
    - export DOCKERFILE=$CI_PROJECT_DIR/root/ci-scripts/Dockerfile.root
    - export CONTEXT=$CI_PROJECT_DIR
    - export IMAGE_NAME=gitlab-registry.cern.ch/hep-benchmarks/hep-analysis-workloads/root
    - export IMAGE_TAG=latest
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - export ROOT_COMMIT_FILE="/hep-analysis-workloads/root/ci-scripts/root_frozen_commit.txt"
    - export ROOT_COMMIT=$(<$ROOT_COMMIT_FILE)
  <<: *build

build_rootbench: 
  stage: build_rootbench
  before_script:
    - export DOCKERFILE=$CI_PROJECT_DIR/root/ci-scripts/Dockerfile.rootbench
    - export CONTEXT=$CI_PROJECT_DIR
    - export IMAGE_NAME=gitlab-registry.cern.ch/hep-benchmarks/hep-analysis-workloads/rootbench
    - export IMAGE_TAG=latest
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - export ROOTBENCH_COMMIT_FILE="/hep-analysis-workloads/root/ci-scripts/rootbench_frozen_commit.txt"
    - export ROOTBENCH_COMMIT=$(<$ROOTBENCH_COMMIT_FILE)
  <<: *build

build_standalone: 
  stage: build_standalone
  before_script:
    - export DOCKERFILE=$CI_PROJECT_DIR/root/ci-scripts/Dockerfile.standalone
    - export CONTEXT=$CI_PROJECT_DIR
    - export IMAGE_NAME=gitlab-registry.cern.ch/hep-benchmarks/hep-analysis-workloads
    - export IMAGE_TAG=${CI_COMMIT_TAG:-$CI_COMMIT_BRANCH}
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
  <<: *build

test_docker: 
  stage: test_docker
  tags:
    - docker-privileged-xl
  image: docker:19.03.1
  services:
    - docker:19.03.1-dind
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
  script:
    - echo $CI_BUILD_TOKEN | docker login -u gitlab-ci-token --password-stdin gitlab-registry.cern.ch
    - docker pull gitlab-registry.cern.ch/hep-benchmarks/hep-analysis-workloads:${CI_COMMIT_TAG:-$CI_COMMIT_BRANCH}
    - docker run -v $PWD:/results gitlab-registry.cern.ch/hep-benchmarks/hep-analysis-workloads:${CI_COMMIT_TAG:-$CI_COMMIT_BRANCH}
  artifacts:
    paths:
      - ./*.json
    expire_in: 1 week
    when: always

test_singularity:
  stage: test_singularity
  tags: 
    - docker-privileged-xl
  image: gitlab-registry.cern.ch/linuxsupport/cc7-base
  dependencies: []
  script:
    - yum install -y -q singularity python3-pip
    - useradd -d $(pwd) -g users -M -N unprivUser
    - chown -R unprivUser:users ..
    - su - unprivUser
    - set -e
    - if [ -d singularity_cachedir ]; then mkdir -p $BMK_VOLUME/singularity_cachedir; mv singularity_cachedir/ $BMK_VOLUME/; fi
    - export SINGULARITY_DOCKER_USERNAME=$CI_REGISTRY_USER
    - export SINGULARITY_DOCKER_PASSWORD=$CI_REGISTRY_PASSWORD
    - singularity run -C --writable-tmpfs -B $PWD:/results docker://gitlab-registry.cern.ch/hep-benchmarks/hep-analysis-workloads:${CI_COMMIT_TAG:-$CI_COMMIT_BRANCH}
  cache:
    key: "$CI_JOB_STAGE-$CI_COMMIT_REF_SLUG"
    paths:
      - singularity_cachedir/cache/**/*
  artifacts:
    paths:
      - ./*.json
    expire_in: 1 week
    when: always
