# hep-analysis-workloads

Standalone ROOT-based HEP analysis workloads for CPU benchmarking purposes, to be integrated in the HEP Benchmarks repository collection: https://gitlab.cern.ch/hep-benchmarks

The Docker/Singularity image runs a subset of the ROOT benchmarks included in rootbench (https://github.com/root-project/rootbench). The benchmarks to run are specified in `/root/root/benchmarks.txt`, along with a "significance" weight asigned to each one (still to decide) in the adjacent column.

The default benchmarks have been selected by the ROOT team, and are considered the most representative and/or relevant for CPU workloads from the available options. In the near future, we will extend them with GPU workload benchmarks.

## Running the benchmarks

In order to run the standalone container, issue the command:

```
docker run gitlab-registry.cern.ch/hep-benchmarks/hep-analysis-workloads
```

The benchmarks' results, in the format expected by the HEP Benchmarks project, can be found in the container's `/results` folder. If you want to access them from outside, please consider bind-mounting your local directory in `/results`:

```
docker run -v <local_dir>:/results gitlab-registry.cern.ch/hep-benchmarks/hep-analysis-workloads
```
